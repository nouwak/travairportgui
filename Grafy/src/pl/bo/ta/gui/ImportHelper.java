package pl.bo.ta.gui;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImportHelper {

	public static List<String[]> getCities() {
		List<String[]> vertexData = new ArrayList<String[]>();
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader("dane.txt"));
			String line = in.readLine();
			while (line != null) {
				String[] splitted = line.split(";");
				vertexData.add(splitted);
				line = in.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println(vertexData);
		return vertexData;
	}

	public static void main(String[] args) {
		getCities();
	}
}
