package pl.bo.ta.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;

public class GraphWindow extends JFrame {

	private List<Object> vertexes = new ArrayList<Object>();
	private List<String[]> citiesData = ImportHelper.getCities();
	private mxGraph graph = new mxGraph() {
		@Override
		public boolean isCellEditable(Object arg0) {
			return false;
		}
	};

	private static final long serialVersionUID = -2707712944901661771L;

	public GraphWindow(JLabel image) {
		super("Travelling Airport v 0.0.1");

		Object parent = graph.getDefaultParent();

		mxStylesheet stylesheet = graph.getStylesheet();
		Hashtable<String, Object> style = new Hashtable<String, Object>();
		style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE);
		style.put(mxConstants.STYLE_FONTCOLOR, "#007917");
		style.put(mxConstants.STYLE_FILLCOLOR, "#ffffdd");
		style.put(mxConstants.STYLE_STROKECOLOR, "#bdfc5b");
		style.put(mxConstants.STYLE_OVERFLOW, "fill");
		stylesheet.putCellStyle("ROUNDED", style);

		graph.getModel().beginUpdate();
		try {
			for (String[] city : citiesData) {
				String nameAndPeople = city[0] + " " + city[1];
				Object v = graph.insertVertex(parent, null, nameAndPeople,
						Double.valueOf(city[2]), Double.valueOf(city[3]),
						6 * (nameAndPeople.length() + 1), 20, "ROUNDED");
				vertexes.add(v);
			}
			// addNextEdge(graph, parent);
		} finally {
			graph.getModel().endUpdate();
		}

		JLayeredPane layeredPane = new JLayeredPane();

		mxGraphComponent graphComponent = new mxGraphComponent(graph);
		graphComponent.setBackgroundImage(new ImageIcon("worldmap.jpg"));
		graphComponent.setConnectable(false);

		JPanel doObrazkow = new JPanel() {
			@Override
			public void paint(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setColor(Color.DARK_GRAY);
				g2d.fillOval(100, 100, 70, 70);
				g2d.fillRect(500, 20, 70, 70);
				//TODO wybuchy wulkanow powinny byc wczytywane z pliku
			}
		};

		layeredPane.add(graphComponent, new Integer(0));
		layeredPane.add(image, new Integer(1));
		layeredPane.add(doObrazkow, new Integer(2));

		graphComponent.setBounds(0, 0, 1200, 778);
		doObrazkow.setBounds(0, 0, 1200, 778);
		doObrazkow.setOpaque(false);
		// image.setBounds(300, 300, 227, 239);

		getContentPane().add(layeredPane);
	}

	private void addNextEdge(int secondVertexNumber, mxGraph graph,
			Object parent) {
		String myEdgeStyle = "defaultEdge;strokeColor=#004405";
		graph.getModel().beginUpdate();
		try {
			if (secondVertexNumber != 0) {
				graph.insertEdge(parent, null, "",
						vertexes.get(secondVertexNumber - 1),
						vertexes.get(secondVertexNumber), myEdgeStyle);
			} else {
				graph.insertEdge(parent, null, "",
						vertexes.get(vertexes.size() - 1), vertexes.get(0),
						myEdgeStyle);
			}
		} finally {
			graph.getModel().endUpdate();
		}

	}

	public static void fly(JLabel image, int x0, int y0, int x1, int y1) {
		int xlen = x1 - x0;
		int ylen = y1 - y0;
		int newx;
		int newy;
		double alpha;
		double length = Math.sqrt(Math.pow(xlen, 2) + Math.pow(ylen, 2));

		if (xlen > 0 && ylen >= 0) {
			alpha = Math.atan((double) ylen / xlen);
		} else if (xlen > 0 && ylen < 0) {
			alpha = Math.atan((double) ylen / xlen) + 2 * Math.PI;
		} else if (xlen < 0) {
			alpha = Math.atan((double) ylen / xlen) + Math.PI;
		} else if (xlen == 0 && ylen > 0) {
			alpha = Math.PI / 2;
		} else {
			alpha = 3 * Math.PI / 2;
		}

		for (int i = 0; i < (int) length; i++) {
			newx = (int) (i * Math.cos(alpha));
			newy = (int) (i * Math.sin(alpha));
			image.setBounds(x0 + newx + 10, y0 + newy - 15, 50, 50);
			try {
				Thread.sleep(4);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void goOnAdventure(JLabel image) {
		List<String[]> citiesData = ImportHelper.getCities();
		for (int i = 1; i < citiesData.size(); i++) {
			fly(image, Integer.valueOf(citiesData.get(i - 1)[2]),
					Integer.valueOf(citiesData.get(i - 1)[3]),
					Integer.valueOf(citiesData.get(i)[2]),
					Integer.valueOf(citiesData.get(i)[3]));
			addNextEdge(i, graph, graph.getDefaultParent());
		}
		fly(image, Integer.valueOf(citiesData.get(citiesData.size() - 1)[2]),
				Integer.valueOf(citiesData.get(citiesData.size() - 1)[3]),
				Integer.valueOf(citiesData.get(0)[2]),
				Integer.valueOf(citiesData.get(0)[3]));
		addNextEdge(0, graph, graph.getDefaultParent());
	}

	public static void main(String[] args) throws InterruptedException {
		JLabel image = new JLabel(new ImageIcon("plane2.png"));

		GraphWindow frame = new GraphWindow(image);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1200, 778);
		frame.setVisible(true);

		frame.goOnAdventure(image);
		// frame.someNew();
	}

}
